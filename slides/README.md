## Introduction

The latest version of the pdf files have been put on googele drive, and can be 
found via the link below. [This move of the files have been done  since the files 
are quite large (and pdf) and they are not suitable for the git repository].<br>

The file <tt>References_Slides_HJ.txt</tt> is a list of references used in the slides. 

## Link to directory with pdf files of the slides

[StudentSlides](https://drive.google.com/drive/folders/15ME7ZJVGqUKyOUiXD3pbGnlnyI9kq4Hk?usp=sharing)

## Contact

henrik.jonsson@slcu.cam.ac.uk

