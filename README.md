## Introduction

A tutorial on modelling growth and cell division aiming to introduce changes to 
deterministic and stochastic models of molecular reaction and regulation models 
that are needed as well as models for growth and cell divisions.

## Folders

This repository contains the following material:

<p> <tt>slides</tt> - folder containing pdf files of the lectures/discussions. 
Note, the latest version of the files are stored on google drive in the folder 
[StudentSlides](https://drive.google.com/drive/folders/15ME7ZJVGqUKyOUiXD3pbGnlnyI9kq4Hk?usp=sharing)</p>

<p> <tt>modelFiles</tt> - example models for simulating growing and dividing cells
using cell-based and vertex-based approaches.</p>

## Tutorials given

This tutorial was originally developed for the Institute of Mathematical Sciences,
National University of Singapore program 
[Quantum and Kinetic Problems: Modeling, Analysis, Numerics and Applications](https://ims.nus.edu.sg/events/2019/quantum/index.php).

## Contact

henrik.jonsson@slcu.cam.ac.uk