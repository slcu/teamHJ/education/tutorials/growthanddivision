## Introduction

This folder have files for simulating growing rod-shaped bacgteria cells
where also a quorum sensing molecular netwotk is implemented.

## Software

To runs simulations, the [Organism](https://gitlab.com/slcu/teamhj/organism) software
is used. To plot the output also the <tt>newman</tt> binary provided in the Organism repository
needs to be compiled.

## Simulations

A simple test simulation can be done using:

<p><tt>ORGANISM/bin/simulator qs.model qs.init qs.rk > qs.data</tt></p>

and the output can be plotted using

<p><tt>ORGANISM/tools/plot/bin/newman -shape cappedCylinder qs.data</tt></p>.

Model parameters can be adjusted in the file <tt>qs.model</tt>, initial state can be adjusted in <tt>qs.init</tt>
and solver parameters as e.g. solver used, length of simulation can be edited in <tt>qs.rk</tt>.

## Reference

Melke, P., Sahlin, P., Levchenko, A. and J&oulm;nsson, H. (2010)
[A Cell-Based Model for Quorum Sensing in Heterogeneous Bacterial Colonies](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2887461/)
PLoS Comp Biol 6: e1000819.

## Contact

henrik.jonsson@slcu.cam.ac.uk
